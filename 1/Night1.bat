@echo off
title Custom Night
set F=0
set B=2
set R=1
set C=2
set energy=100
set consumption=5
set power=%consumption%
set Froom=1C
set Rroom=1A
set Broom=1A
set Croom=1A
set clock=300
set LD=
set RD=
set LDoor=1
set RDoor=1
set camera=1
set AAr=1A
set ABr=1B
set BAr=2A
set BBr=2A
set Cr=3
set ACr=1C
set Er=5
set DAr=4A
set DBr=4B
set Fr=6
set Gr=7
goto Office

:Office
color 07
if %clock%==0 goto Win
cls
echo Foxy  %Froom%  Chica %Croom%  Freddy  %Rroom%  Boney  %Broom%
echo %energy%    %clock%
echo                      _________
echo                     I   1A    I
echo  ______    _________I         I______
echo I      I__I                          I   ___
echo I   5   __                           I__I   I  ____
echo I      I  I                           __  7 I_I    I
echo I______I  I                          I  I    _     I
echo           I                          I  I   I I____I
echo       ____I             1B           I  I   I
echo      I    I                          I  I   I
echo      I 1C I                          I  I   I_I   I
echo      I____I                          I  I    _    I
echo           I__     __________     __  I  I___I I___I
echo     ______   I2A I          I 4AI _I I________
echo    I      I__I   I          I   II            I
echo    I       __    I          I   II      6     I
echo    I      I  I   I  ______  I   II            I
echo    I   3  I  I   I_I      I_I   II____________I
echo    I______I  I2B  _%LD%  YOU   %RD%_  4BI
echo              I___I I______I I___I
choice /c sxad /n /t 1 /d x 
if %errorlevel%==1 goto Camera1
if %errorlevel%==2 goto Timer
if %errorlevel%==3 goto DoorL
if %errorlevel%==4 goto DoorR
goto Timer

:Camera1
set camera=2
goto Camera

:Timer
set /a clock-=1
goto AI
:P
set /a power-=1
echo %power%
if %power% NEQ 0 goto Where
set /a energy-=1
set power=%consumption%
goto Where

:Where
if %Froom%==YOU goto Check
:Whereb
if %camera%==2 goto Camera
if %camera%==1 goto Office

:Check
if %Broom%==YOU goto Check2

:Check2
if %Croom%==YOU goto Check3

:Check3
if %Rroom%==YOU goto DEAD
goto Whereb

:AI
set /a num=%random% %%20 +1
if %num% lss %F% goto Foxy
:B
set /a num=%random% %%20 +1
if %num% lss %B% goto Boney
:C
set /a num=%random% %%20 +1
if %num% lss %C% goto Chica
:R
set /a num=%random% %%20 +1
if %num% lss %R% goto Freddy
goto ROOM

:Foxy
if %camera%==2 goto B
if %Froom%==3 goto Frun
if %Froom%==2 set Froom=3
if %Froom%==1 set Froom=2
if %Froom%==1C set Froom=1
goto B

:Frun
start vlc -I null --play-and-exit --volume=200 "%rootdir%\Sound\FoxyRun.mp3"
timeout /t 3 /nobreak>nul
set Froom=YOU
if %LDoor%==2 set Froom=1C
goto B

:Boney
if %Broom%==2B goto BDead
set /a num=%random% %%5 +1
if %num%==1 set Broom=1A
if %num%==2 set Broom=1B
if %num%==3 set Broom=2A
if %num%==4 set Broom=2B
if %num%==5 set Broom=3
goto C

:BDead
set Broom=YOU
if %LDoor%==2 set Broom=1A 
goto C

:Chica
if %Croom%==4B goto CDead
if %Croom%==4A goto Chica4A
if %Croom%==1B goto Chica1B
if %Croom%==6 set Croom=1B
if %Croom%==1A set Croom=1B
goto R

:Chica1B
set /a num=%random% %%3 +1
if %num%==1 set Croom=1A
if %num%==2 set Croom=4A
if %num%==3 set Croom=6
goto R

:Chica4A
set /a num=%random% %%3 +1
if %num%==1 set Croom=4B
if %num%==2 set Croom=1B
if %num%==3 set Croom=6
goto R

:CDead
set Croom=YOU
if %RDoor%==2 set Croom=1A
goto R

:Freddy
if %Rroom%==2B goto RDead
if %RRoom%==2A set Rroom=2B
if %Rroom%==1B goto Freddy1B
if %Rroom%==1A set Rroom=1B
goto ROOM

:RDead
set Rroom=YOU
if %LDoor%==2 set Rroom=1A
goto ROOM

:Freddy1B
set /a num=%random% %%2 +1
if %num%==1 set Rroom=1A
if %num%==2 set Rroom=2A
goto ROOM

:Win
cls
echo Winning
pause
exit 

:Camera
set consumption=2
color 0A
cls
echo %energy%    %clock%
echo Foxy  %Froom%  Chica %Croom%  Freddy  %Rroom%  Boney  %Broom%
echo                      _________
echo                     I   %AAr%    I
echo  ______    _________I         I______
echo I      I__I                          I   ___
echo I   %Er%   __                           I__I   I  ____
echo I      I  I                           __  %Gr% I_I    I
echo I______I  I                          I  I    _     I
echo           I                          I  I   I I____I
echo       ____I             %ABr%           I  I   I
echo      I    I                          I  I   I
echo      I %ACr% I                          I  I   I_I   I
echo      I____I                          I  I    _    I
echo           I__     __________     __  I  I___I I___I
echo     ______   I%BAr% I          I %DAr%I _I I________
echo    I      I__I   I          I   II            I
echo    I       __    I          I   II      %Fr%     I
echo    I      I  I   I  ______  I   II            I
echo    I   %Cr%  I  I   I_I      I_I   II____________I
echo    I______I  I%BBr%  _%LD%  YOU   %RD%_  %DBr%I
echo              I___I I______I I___I
choice /c sxad /n /t 1 /d x 
if %errorlevel%==1 goto Camera2
if %errorlevel%==2 goto Timer
if %errorlevel%==3 goto DoorL
if %errorlevel%==4 goto DoorR
goto Timer

:DoorR
if %RDoor%==1 goto DoorR1
if %RDoor%==2 set RDoor=1
set RD=
goto Where

:DoorR1
set RDoor=2
set consumption=1
set RD=I
goto Where

:DoorL
if %LDoor%==1 goto DoorL1
if %LDoor%==2 set LDoor=1 
set LD=
goto Where

:DoorL1
set LDoor=2
set consumption=1
set LD=I
goto Where

:Camera2
set camera=1
goto Office

:ROOM
if %Rroom%==1A goto F1Aroom
if %Croom%==1A goto 1Aroom
if %Broom%==1A goto Boney1Ar
set AAr=1A
goto 2ARm

:Boney1Ar
set AAr=Bn
goto 2ARm

:2ARm
if %Rroom%==1B goto F1Broom
if %Croom%==1B goto 2Broom
if %Broom%==1B goto Boney1Br
set ABr=1B
goto 4ARm

:Boney1Br
set ABr=Bn
goto 4ARm

:4ARm
if %Croom%==4A set DAr=Ch
if %Croom%==4B set DBr=Ch
if %Croom%==6 set Fr=C
if %Rroom%==2A goto 2Aroom
if %Croom% NEQ 4A set DAr=4A
if %Croom% NEQ 4B set DBr=4B
if %Croom% NEQ 6 set Fr=6
if %Rroom% NEQ 2A set BAr=2A
if %Broom%==2A set ABr=Bn
if %Broom% NEQ 2A set ABr=2A
goto 2BRm


:2BRm
if %Rroom%==2B goto 2Broom
if %Rroom% NEQ 2B set BBr=2B
if %Broom%==2B set BBr=Bn
if %Broom% NEQ 2B set BBr=2B
goto OtherR

:OtherR
if %Broom%==3 set Cr=B
if %Broom% NEQ 3 set Cr=3
if %Froom%==1C set ACr=Fo
if %Froom%==1 set ACr=F1
if %Froom%==2 set ACr=F2
if %Froom%==3 set ACr=1C
goto P

:F1Broom
if %Croom%==1B goto FC1Broom
if %Broom%==1B set AAB=FB
goto 4ARm

:FC1Broom
set ABr=FC
if %Broom%==1B set ABr=FCB
goto 4ARm

:2Aroom
set 2Ar=Fr
if %Rroom%==2A set AAr=FB
goto 2BRm

:2Broom
set 2Br=Fr
if %Rroom%==2B set BBr=FB
goto OtherR

:1Aroom
set AAr=Ch
if %Rroom%==1A set AAr=CB
goto 2ARm

:F1Aroom
if %Croom%==1A goto FC1Aroom
if %Broom%==1A set AAr=FB
goto 2ARm

:FC1Aroom
set AAr=FC
if %Broom%==1A set AAr=FCB
goto 2ARm

:DEAD
cd %rootdir%\DEAD
call Dead.bat