@echo off
cls
title FNAF batch files
set F=1
set R=1
set C=1
set B=1
set /a num=%random% %%2 +1
set rootdir=%cd%
type Warning.txt
echo If the audio doesn't turn off.
echo Use the taskmanager and end it there.
pause
if %num%==1 goto Song1
if %num%==2 goto Song2

:Song1
start vlc -I null --play-and-exit --volume=200 "%rootdir%\Song1.ogg"
goto Menu

:Song2
start vlc -I null --play-and-exit --volume=50 "%rootdir%\Song2.mp3"
goto Menu

:Menu
cls
type Menu.txt
set /p answer=: 
if %answer%==q goto Quit
if %answer%==p goto Play
goto Menu

:Play
cls
type Characters.txt
set /p answer=
if %answer%==f goto Foxy
if %answer%==r goto Freddy
if %answer%==c goto Chica
if %answer%==b goto Boney
if %answer%==bc goto Menu
if %answer%==q goto Quit
if %answer%==go goto Begin
if %answer%==1 goto Night1
if %answer%==2 goto Night2
if %answer%==3 goto Night3
if %answer%==4 goto Night4
if %answer%==5 goto Night5
goto Play

:Foxy
cls
echo When you're ready write go in the select screen.
echo Enter number from 1-20.
echo 20 being the hardest and 1 being the easiest.
echo If you enter anything else you may break the game.
set /p answer=Difficulty: 
set F=%answer%
goto Play

:Freddy
cls
echo When you're ready write go in the select screen.
echo Enter number from 1-20.
echo 20 being the hardest and 1 being the easiest.
echo If you enter anything else you may break the game.
set /p answer=Difficulty: 
set R=%answer%
goto Play

:Chica
cls
echo When you're ready write go in the select screen.
echo Enter number from 1-20.
echo 20 being the hardest and 1 being the easiest.
echo If you enter anything else you may break the game.
set /p answer=Difficulty: 
set C=%answer%
goto Play

:Boney
cls
echo When you're ready write go in the select screen.
echo Enter number from 1-20.
echo 20 being the hardest and 1 being the easiest.
echo If you enter anything else you may break the game.
set /p answer=Difficulty: 
set B=%answer%
goto Play

:Begin
cls
taskkill /F /IM vlc.exe /T
cd %rootdir%\Custom
call Custom.bat

:Night1
cls
taskkill /F /IM vlc.exe /T
cd %rootdir%\1
call Nigh1.bat

:Night2
cls
taskkill /F /IM vlc.exe /T
cd %rootdir%\2
call Night2.bat

:Night3
cls
taskkill /F /IM vlc.exe /T
cd %rootdir%\3
call Night3.bat

:Night4
cls
taskkill /F /IM vlc.exe /T
cd %rootdir%\4
call Night4.bat

:Night5
cls
taskkill /F /IM vlc.exe /T
cd %rootdir%\5
call Night5.bat

:Quit
taskkill /F /IM vlc.exe /T
exit